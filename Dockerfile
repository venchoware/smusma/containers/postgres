FROM postgres:12-alpine
LABEL maintainer="michael.sullivan@venchoware.com"
COPY *.sh *.sql /docker-entrypoint-initdb.d/
