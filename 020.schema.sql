create table crawls (
	vendor varchar(40),
	start timestamp,
	finish timestamp,
	duration interval
)
